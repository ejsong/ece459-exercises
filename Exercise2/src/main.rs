// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    match n {
        0 => 0,
        1 | 2 => 1,
        3 => 2,
        _ => {
            let mut iter = 3;
            let mut small_fib = 1;
            let mut large_fib = 2;
            while iter < n {
                large_fib = large_fib + small_fib;
                small_fib = large_fib - small_fib;
                iter += 1;
            }

            large_fib
        }
    }
}


fn main() {
    println!("{}", fibonacci_number(10));
}
