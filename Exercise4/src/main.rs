use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let tx1 = mpsc::Sender::clone(&tx);
    thread::spawn(move || {
        let message = String::from("First thread message");
        tx.send(message)
            .expect("Error sending message from first thread");
    });

    thread::sleep(Duration::from_secs(1));

    thread::spawn(move || {
        let message = String::from("Second thread message");
        tx1.send(message)
            .expect("Error sending message from second thread");
    });

    for received in rx {
        println!("Got: {}", received);
    }
}
