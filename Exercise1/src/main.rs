// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut offset = 0;
    let mut ans = 0;
    while offset < number {
        if offset + multiple1 * multiple2 > number {
            let mut temp = offset + multiple1;
            while temp < number {
                ans += temp;
                temp += multiple1;
            }
            temp = offset + multiple2;
            while temp < number {
                ans += temp;
                temp += multiple2;
            }
            break;
        } else {
            for i in 0 .. multiple2 {
                ans += multiple1 * (i+1) + offset;
            }
            
            for i in 0 .. multiple1-1 {
                ans += multiple2 * (i+1) + offset;
            }
        }

        offset += multiple1 * multiple2;
    }

    ans
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
