use std::thread;

static N: i32 = 10; // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for _ in 0..N {
        let handle = thread::spawn(|| {
            println!("Thread finished");
        });
        children.push(handle);
    }

    for spawned_thread in children.into_iter() {
        spawned_thread.join().expect("Join error");
    }
    println!("Main thread finished");
}
